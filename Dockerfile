FROM ubuntu:artful
RUN apt update -y && apt install -y git build-essential python-pip php && \
    cd root && git clone https://github.com/otsuarez/mkdocs_auth.git &&\
    cd mkdocs_auth &&\
    pip install -r requirements.txt
COPY ./base.html /root/mkdocs_auth/theme_addons/base.html
RUN pip install pygments pymdown-extensions